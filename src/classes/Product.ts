export type Dimensions = {

  length: number,
  width: number,
  height: number,
}

class Product {

  constructor(
    public productId: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions
  ) { }

  displayDetails(): string {

    let description = `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} € `;
    description += `Dimensions: ${this.dimensions.length}, ${this.dimensions.width}, ${this.dimensions.height}`
    return description;
  }
}

export default Product;