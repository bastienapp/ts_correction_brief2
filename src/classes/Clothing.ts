import { ClothingSize } from "../enums/ClothingSize";
import Product, { Dimensions } from "./Product";

class Clothing extends Product {

  size: ClothingSize;

  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimensions: Dimensions,
    size: ClothingSize
  ) {
    super(productId, name, weight, price, dimensions)
    this.size = size;
  }

  displayDetails(): string {

    let description = super.displayDetails();
    description += ' - Size: ' + this.size;

    return description;
  }
}

export default Clothing;