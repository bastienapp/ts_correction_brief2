import { ShoeSize } from "../enums/ShoeSize";
import Product, { Dimensions } from "./Product";

export default class Shoes extends Product {

  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimensions: Dimensions,
    public size: ShoeSize
  ) {
    super(productId, name, weight, price, dimensions)
  }

  displayDetails(): string {

    let description = super.displayDetails();
    description += ' - Size: ' + this.size;

    return description;
  }
}