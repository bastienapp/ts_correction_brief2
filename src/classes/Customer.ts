export type Address = {

  street: string,
  city: string,
  postalCode: string,
  country: string,
}

class Customer {

  constructor(
    public customerId: number,
    public name: string,
    public email: string,
    public address?: Address
  ) { }

  displayInfo(): string {

    let description = `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email} - `;
    description += this.displayAddress();

    return description;
  }

  displayAddress(): string {
    if (this.address) {
      return `Address: ${this.address.street}, ${this.address.postalCode} ${this.address.city}, ${this.address.country}`;
    }
    return 'No address found';
  }

  setAddress(newAddress: Address): void {
    this.address = newAddress;
  }
}

export default Customer;