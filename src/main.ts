import Clothing from "./classes/Clothing";
import Customer from "./classes/Customer";
import Product, { Dimensions } from "./classes/Product";
import Shoes from "./classes/Shoes";
import { ClothingSize } from "./enums/ClothingSize";
import { ShoeSize } from "./enums/ShoeSize";

const michel = new Customer(14, "Michel Durand", "michel@wanadoo.fr");

michel.setAddress({
  street: '13 rue des pyrénées',
  city: 'Nantes',
  postalCode: '34000',
  country: 'France',
});
console.log(michel);
console.log(michel.displayInfo());

const gege = new Customer(57, "Gérard", "gege@htomail.com");
console.log(gege.displayInfo());

const shoesDimensions: Dimensions = {
  length: 80,
  height: 26,
  width: 42,
};
const adidas = new Product(477, "Adidas Gazelle", 0.8, 59, shoesDimensions);

console.log(adidas.displayDetails());

const tshirt = new Clothing(47, 'T-shirt bleu', 0.2, 12, { height: 65, width: 84, length: 0.4}, ClothingSize.SIZE_L);

console.log(tshirt.displayDetails());

const nike = new Shoes(8995, 'Nike Air Jordan', 1.2, 69, { height: 90, width: 45, length: 74}, ShoeSize.SIZE_45);

console.log(nike.displayDetails());