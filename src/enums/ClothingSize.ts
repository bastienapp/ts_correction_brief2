export enum ClothingSize {

  SIZE_XS = 'XS',
  SIZE_S = 'S',
  SIZE_M = 'M',
  SIZE_L = 'L',
  SIZE_XL = 'XL',
  SIZE_XXL = 'XXL',
}