# TypeScript - Programmation orientée objet

Apprends les bases de la programmation orientée objet en TypeScript avec un projet.

## Ressources

- [TypeScript Documentation - Classes](https://www.typescriptlang.org/fr/docs/handbook/2/classes.html)
- [Codecademy - TypeScript Classes](https://www.codecademy.com/resources/docs/typescript/classes)
- [Grafikart - Les classes en TypeScript](https://www.youtube.com/watch?v=MlwFhfdEVwo)

## Contexte du projet

Tu vas travailler pour SimplonFactory, une entreprise innovante dans le secteur de la fabrication et de la vente en ligne.

Ta mission est de créer un système qui gère les commandes des clients, le catalogue de produits, et le processus de livraison. Ce système devra permettre de créer et de gérer des clients (Customer), de gérer un inventaire de produits (Product) avec des spécificités telles que les vêtements (Clothing) et les chaussures (Shoes), et de gérer des commandes (Order) avec une logique de livraison flexible (Deliverable).

## Modalités pédagogiques

### 1. Classes

Chaque classe sera créée avec des propriétés publiques (pas besoin de faire de getters / setters) et un constructeur renseignant les valeurs des propriétés.

**Classe `Customer` :**

- Propriétés :
	- `customerId` (numérique)
	- `name` (texte)
	- `email` (texte)
- Méthodes :
	- `displayInfo(): string`: Retourne les informations d'un client au format "Customer ID: $customerId, Name: $name, Email: $email"

**Classe `Product` :**

- Propriétés :
	- `productId` (numérique)
	- `name` (texte)
	- `weight` (numérique)
	- `price` (numérique)
- Méthodes :
	- `displayDetails(): string` : Retourne les détails d'un produit au format "Product ID: $productId, Name: $name, Weight: $weight, Price: $price €"

### 2. Types

**Type `Dimensions` :** length (numérique), width (numérique), height (numérique).

- Ajouter la propriété `dimensions` à la classe `Product`, de type `Dimensions`.
- Modifier le constructeur de `Product` afin que la dimension soit renseignée.
- Modifier la méthode `displayDetails()` afin d'ajouter le détail des dimensions lors du retour de la méthode.

**Type `Address` :** `street` (texte), `city` (texte), `postalCode` (texte), `country` (texte).

- Ajouter la propritété `address` dans la classe `Customer`, de type `Address | undefined`.
- Inutile de modifier le constructeur de `Customer`.
- Ajouter la méthode `setAddress(address: Address)` : Renseigne l'adresse du client
- Ajouter la méthode `displayAddress(): string` : Retourne les informations de l'adresse au format : "Address: $street, $postalCode $city, $country" si l'adresse existe, ou "No address found" sinon.
- Modifier la méthode `displayInfo()` afin d'ajouter les informations de l'adresse.

### 3. Enums

**Enum `ClothingSize`** : aura comme valeurs XS, S, M, L, XL, XXL.

**Enum `ShoeSize`** : aura comme valeurs 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46.

### 4. Héritage

**Sous-classes de `Product` :** `Clothing`, `Shoes`.

- Pour `Clothing`, ajouter la propriété `size`, de type `ClothingSize`.
- Pour `Shoes`, ajouter la propriété `size`, de type `ShoeSize`.
- Redéfinir les constructeurs pour chaque sous-classe afin de renseigner les valeurs des propriétés.
- Redéfinir `displayDetails()` pour chaque sous-classe pour inclure les nouvelles propriétés.

### 5. Méthodes

**Classe `Order` :**

- Propriétés : `orderId` (numérique), `customer` (instance de `Customer`), `productsList` (tableau de `Product`), `orderDate` (date).
- Méthodes :
   - `addProduct(product: Product)`: Ajoute un produit à la commande.
   - `removeProduct(productId: number)`: Retire un produit de la commande.
   - `calculateWeight()`: Calcule le poids total de la commande.
   - `calculateTotal()`: Calcule le prix total de la commande.
   - `displayOrder()`: Affiche les détails de la commande : les informations de l'utilisateur, les informations de chaque produit et le total de la commande.

### 6. Interfaces

**Interface `Deliverable` :**
- Méthodes :
	- `estimateDeliveryTime(weight: number): number` : Estime le délai de livraison en nombre de jours.
	- `calculateShippingFee(weight: number): number`: Calcule les frais de livraison.

**Classes implémentant `Deliverable` :** `StandardDelivery` et `ExpressDelivery`.

Chaque classe doit implémenter les méthodes de `Deliverable` :

**`StandardDelivery` :**
- `estimateDeliveryTime` :
	- `< 10kg` : retourne 7
	- sinon retourne 10
- `calculateShippingFee` :
	- `< 1kg` : retourne 5
	- `entre 1kg et 5kg` : retourne 10
	- `> 5kg` : retourne 20

**`ExpressDelivery` :**
- `estimateDeliveryTime` :
	- `<= 5kg` : retourne 1
	- sinon retourne 3
- `calculateShippingFee` :
	- `< 1kg` : retourne 8
	- `entre 1kg et 5kg` : retourne 14
	- `> 5kg` : retourne 30

5. **Modification de la classe `Order` :**

- Ajouter une propriété `delivery: Deliverable | undefined`.
- Ne pas modifier le constructeur.
- Ajouter une méthode `setDelivery(delivery: Deliverable)` : Permet d'assigner un service de livraison à la commande.
- Ajouter une méthode `calculateShippingCosts()`: Utilise la méthode `calculateShippingFee` de l'objet `delivery` pour calculer les frais de livraison en fonction du poids total de la commande.
- Ajouter une méthode `estimateDeliveryTime()`: Utilise la méthode `estimateDeliveryTime` de l'objet `delivery` pour estimer le délai de livraison de la commande.

## Modalités d'évaluation

- L'application doit être entièrement écrite en TypeScript, en programmation orientée objet.
- Ne pas utiliser d'outil d'intelligence artificielle pour générer le code (ex : Copilot, ChatGPT). Le but est d'apprendre les concepts nécessaires au fur et à mesure, pas juste de produire la solution de l'exercice.
- Commenter le code pour expliquer les choix de conception et faciliter la compréhension et la maintenance.

## Livrables

- Un lien vers GitLab.
- Un fichier README expliquant comment installer et exécuter l'application.
- Des exemples d'utilisation de l'application pour montrer son fonctionnement.
- Des commentaires expliquent le code.

## Critères de performance

- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions
